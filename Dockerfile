FROM julia:1.7

COPY launch_shuffler.jl launch_shuffler.jl
COPY Project.toml Project.toml

RUN julia --project -e 'using Pkg; Pkg.add(url="https://github.com/meggart/DiskArrayShufflers.jl"); Pkg.resolve(); Pkg.precompile()'

EXPOSE 8081

ENTRYPOINT ["julia", "--project", "./launch_shuffler.jl"]
#CMD bin/bash